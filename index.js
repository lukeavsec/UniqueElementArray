export default iterable => [ ...new Set(iterable) ]
